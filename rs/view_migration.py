from django.db import connection


def create_keywords_view(**kwargs):
    cursor = connection.cursor()
    # create or replace
    cursor.execute("create or replace view tmdb_movie_keywords_1 as SELECT tmdb_movie_keywords.keyword_id, rs_vote.user_id, AVG(rs_vote.value) AS value FROM rs_vote JOIN tmdb_movie_keywords ON rs_vote.movie_id = tmdb_movie_keywords.movie_id GROUP BY tmdb_movie_keywords.keyword_id, rs_vote.user_id")

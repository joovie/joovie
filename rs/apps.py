from django.apps import AppConfig

from rs.view_migration import create_keywords_view
from django.db.models.signals import post_migrate


class RsConfig(AppConfig):
    name = 'rs'
    verbose_name = 'Recommendation System'

    def ready(self):
        post_migrate.connect(create_keywords_view, sender=self)

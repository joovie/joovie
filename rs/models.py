from django.contrib.auth.models import User
from django.db import models

from tmdb.models import Movie


class Vote(models.Model):
    """
    Saves user votes for movies
    to allow customers to create custom ratings
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, default=None)

    # Integer number between 0 and 10
    # where 0 is ban of movie, and others are actual vote
    value = models.IntegerField()

    def __str__(self):
        return '%s.0 for %s by %s' \
               % (self.value, self.movie.title, self.user.username)

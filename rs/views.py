from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import View
from django.views.generic import ListView

from rs.models import Vote


class VoteView(View):
    def post(self, request, movie_id):
        """
        Saves user's vote for specified movie.
        Returns 'OK' if AJAX or redirects to previous page otherwise.
        """
        vote, created = Vote.objects.update_or_create(
            user=request.user,
            movie__id=movie_id,
            defaults={
                'movie_id': movie_id,
                'value': request.POST.get('value'),
            },
        )

        # Process AJAX call
        if request.is_ajax():
            return HttpResponse('Vote %s accepted' % vote)

        # Process regular call
        nxt = request.POST.get('next', '')
        if not nxt:
            nxt = request.META.get('HTTP_REFERER')

        return redirect(nxt)


class VoteListView(ListView):
    model = Vote
    template_name = 'rs/vote_list.jinja2'

    def get_queryset(self):
        blocked = Vote.objects.filter(
                user=self.request.user,
                value = 0
            ).values('movie_id')
        return super(VoteListView, self).get_queryset() \
            .filter(user=self.request.user) \
            .exclude(movie_id__in=blocked)

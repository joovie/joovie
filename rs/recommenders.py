import random

from rs.models import Vote
from tmdb.models import Movie


class ContentBased:
    """
    Acquiring recommendation using content-based algorithm.
    Returns queryset of movies, recommended to @user.
    """

    def get(self, user):
        user_id = user.id
        qs = Movie.objects.extra(
            select={
                'val': 'SUM(tmdb_movie_keywords_1.value) / (SELECT MAX(rs_vote.value) FROM rs_vote WHERE rs_vote.user_id=%s) / sqrt((SELECT COUNT(*) FROM tmdb_movie_keywords WHERE tmdb_movie_keywords.movie_id = tmdb_movie.id) * (SELECT COUNT(DISTINCT tmdb_movie_keywords.keyword_id) FROM rs_vote JOIN tmdb_movie_keywords ON rs_vote.movie_id = tmdb_movie_keywords.movie_id WHERE rs_vote.user_id = %s))'},
            select_params=[user_id, user_id],
            tables=['tmdb_movie_keywords', 'tmdb_movie_keywords_1',],
            where=['tmdb_movie.id = tmdb_movie_keywords.movie_id',
                   'tmdb_movie_keywords.keyword_id = tmdb_movie_keywords_1.keyword_id',
                   'tmdb_movie_keywords_1.user_id=%s'],
            params=[user_id]
        ).order_by('-val')
        qs.query.group_by = ['id']
        votes = Vote.objects.filter(user=user).values('movie_id')
        return qs.exclude(id__in=votes)


class UserBased:
    """
    Acquiring recommendation using user-based (collaborative) algorithm.
    Returns queryset of movies, recommended to @user.
    """

    def get(self, user):
        user_id = user.id
        qs = Movie.objects.extra(
            select={
                'val': '-MIN((SELECT avg(((another_user.value - curr_user.value) * (another_user.value - curr_user.value)) / 100) / COUNT(*) * sqrt( ( SELECT COUNT(*) FROM rs_vote WHERE user_id = %s ) * ( SELECT count(*) FROM rs_vote WHERE user_id = auth_user.id ) ) AS coef FROM rs_vote AS another_user, rs_vote AS curr_user WHERE another_user.movie_id = curr_user.movie_id AND curr_user.user_id = auth_user.id AND another_user.user_id = %s AND auth_user.id <> %s GROUP BY auth_user.id )/rs_vote.value)'},
            select_params=[user_id, user_id, user_id],
            tables=['auth_user', 'rs_vote', ],
            where=['rs_vote.movie_id = tmdb_movie.id', 'rs_vote.user_id = auth_user.id', ]
        ).order_by('-val')
        qs.query.group_by = ['id']
        votes = Vote.objects.filter(user=user).values('movie_id')
        return qs.exclude(id__in=votes)


def getSortingField(item):
    return -item.val


class Recommender:
    """
    Acquiring generalized recommendation.
    Returns list of @number of movies, recommended to @user.
    Use just to output. Inapplicable to use when filtering.
    """

    def get(self, user, number):
        user_based = UserBased().get(user)[:number]
        content_based = ContentBased().get(user)[:number]
        res = list(user_based) + list(content_based)
        res = list(set(res))
        rand_smpl = [res[i] for i in sorted(random.sample(range(len(res)), number))]
        sorted_res = sorted(rand_smpl, key=getSortingField)
        return sorted_res


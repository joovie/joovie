from django.conf.urls import url

from rs.views import *

urlpatterns = [
    url(r'^movie/(?P<movie_id>\d+)/vote', VoteView.as_view(), name='vote'),
    url(r'^user/votes', VoteListView.as_view(), name='vote_list'),
]

import django_filters

from tmdb.models import Movie


class MovieFilter(django_filters.FilterSet):
    class Meta:
        model = Movie
        fields = {
            'title': ['icontains'],
            'release_date': ['year__gte', 'year__lte'],
            'runtime': ['gte', 'lte'],
            'companies': ['exact'],
            'countries': ['exact'],
            'budget': ['gte', 'lte'],
            'revenue': ['gte', 'lte'],
            'genres': ['exact'],
            'keywords': ['exact'],
            'vote_average': ['gte', 'lte'],
        }

from django.db.models import Min, Max, ExpressionWrapper, F, DecimalField
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView

from rs.models import Vote
from search.filters import MovieFilter
from tmdb.models import Movie


class SearchView(ListView):
    model = Movie
    template_name = 'search/search.jinja2'
    paginate_by = 36

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.filter = None

    def get_queryset(self):
        if self.filter is None:
            self.filter = MovieFilter(self.request.GET,
                                      queryset=Movie.objects.annotate(
                                          order_value=ExpressionWrapper(F('vote_average') - 1000 / F('vote_count'),
                                                                        output_field=DecimalField())) \
                                      .order_by('-order_value')
                                      )
        return self.filter.qs

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['filter'] = self.filter
        context['min_year'] = Movie.objects.filter(release_date__isnull=False).order_by('release_date')[
            0].release_date.year
        context['max_year'] = Movie.objects.filter(release_date__isnull=False).latest('release_date').release_date.year
        context['min_runtime'] = Movie.objects.filter(runtime__isnull=False).order_by('runtime')[0].runtime
        context['max_runtime'] = Movie.objects.filter(runtime__isnull=False).latest('runtime').runtime
        context['min_budget'] = Movie.objects.filter(budget__isnull=False).order_by('budget')[0].budget
        context['max_budget'] = Movie.objects.filter(budget__isnull=False).latest('budget').budget
        context['min_revenue'] = Movie.objects.filter(revenue__isnull=False).order_by('revenue')[0].revenue
        context['max_revenue'] = Movie.objects.filter(revenue__isnull=False).latest('revenue').revenue

        if not self.request.user.is_authenticated():
            return context

        context['vote_dict'] = {}
        for movie in context['object_list']:
            try:
                context['vote_dict'][movie.pk] = Vote.objects.get(
                    user=self.request.user,
                    movie=movie
                ).value
            except Vote.DoesNotExist:
                pass

        return context

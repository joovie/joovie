"""TMDb Application Models

class Movie(models.Model) - main structural unit of TMDb's representation
and start point of module. Other models depends on Movie.
"""
from django.db import models
from django.db.models import Q
from select2 import fields
from django.contrib.auth.models import User


class Character(models.Model):
    """
    Stores information about movie characters and actor they are performed by
    """
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE, default=None)
    character = models.TextField(null=True, blank=True)
    actor = models.TextField()

    # Proposes special order of characters within a movie
    order = models.IntegerField()

    picture = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s (%s)' % (self.actor, self.character)


class Company(models.Model):
    """
    Information about the company that produced the movie
    """
    id = models.IntegerField(primary_key=True)
    name = models.TextField()

    def __str__(self):
        return self.name


class Country(models.Model):
    """
    Information about country, in which the movie was produced
    """

    # ISO country code
    # from ISO 3166-1 - Codes for the representation of names
    # of countries and their subdivisions – Part 1: Country codes
    # More: https://en.wikipedia.org/wiki/ISO_3166-1
    code = models.CharField(primary_key=True, max_length=6)

    # Name of country in English
    name = models.TextField()

    def __str__(self):
        return "%s (%s)" % (self.name, self.code)


class Genre(models.Model):
    """
    Stores information about genre of movie
    """
    id = models.IntegerField(primary_key=True)
    name = models.TextField()

    def __str__(self):
        return self.name


class Keyword(models.Model):
    """
    Stores word for movie search
    """
    id = models.IntegerField(primary_key=True)
    name = models.TextField()  # as like as in Genre

    def __str__(self):
        return self.name


class Movie(models.Model):
    """
    Information about movie. Main structural unit
    """
    id = models.IntegerField(primary_key=True)

    # General
    title = models.TextField()
    original_title = models.TextField()
    original_language = models.TextField()
    release_date = models.DateField(null=True, blank=True)
    overview_en = models.TextField(null=True, blank=True)
    runtime = models.IntegerField(null=True, blank=True)

    # Creators
    companies = fields.ManyToManyField(
        Company,
        ajax=True,
        search_field=lambda q: Q(name__icontains=q),
        js_options={'quiet_millis': 200}
    )
    countries = fields.ManyToManyField(
        Country,
        ajax=True,
        search_field=lambda q: Q(name__icontains=q),
        js_options={'quiet_millis': 200}
    )

    # Financial info
    budget = models.FloatField(null=True, blank=True)
    revenue = models.FloatField(null=True, blank=True)

    # Websites
    homepage = models.TextField(null=True, blank=True)
    imdb_id = models.CharField(max_length=10, null=True, blank=True)

    # Categorization
    genres = fields.ManyToManyField(
        Genre,
        ajax=True,
        search_field='name',
        js_options={'quiet_millis': 200}
    )
    keywords = fields.ManyToManyField(
        Keyword,
        ajax=True,
        search_field=lambda q: Q(name__icontains=q),
        js_options={'quiet_millis': 200}
    )

    # User votes (dumped from TMDb)
    vote_count = models.IntegerField()
    vote_average = models.FloatField()

    # Unique views on the TMDb website but improved with taking in
    # account the number of ratings a movie received, number of favourites
    # and number of watched list additions all for the previous day.
    # It also uses the part of the previous days score to help popularity
    # trending and finally, boosts scores a tad if the newer a release date is.
    popularity = models.FloatField()

    # Media
    poster_path = models.TextField(null=True, blank=True)
    backdrop_path = models.TextField(null=True, blank=True)

    # Special marks
    adult = models.BooleanField()

    # Current status of the movie (one of following):
    # - Released
    # - In Production
    # - Planned
    # - Post Production
    # - Rumored/Canceled
    status = models.TextField()

    # Recommendation System (RS) fields
    # ... goes here

    def __str__(self):
        return "%s (%d)" % (self.title, self.release_date.year)

    @property
    def get_trailer(self):
        """
        Returns main trailer video for movie.
        :return: object of models.Video or None, if it absent
        """
        trailers = self.video_set.filter(type='Trailer')
        if len(trailers) > 0:
            return trailers[0]

    def get_trailer_list(self):
        """
        Shortcut for list of trailers for this movie
        :return: QuerySet of models.Video
        """
        return self.video_set.filter(type='Trailer')


class Video(models.Model):
    """
    Stores link on trailer or film.
    """
    id = models.CharField(primary_key=True, max_length=200)

    # Relation with the model of the film that describes
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, default=None)

    # Unique ID of video file or page at external website
    key = models.CharField(max_length=200, null=True, blank=True)

    name = models.TextField()

    # Name of site where video is stored
    site = models.TextField()

    # Video quality in pixels of height
    size = models.IntegerField()

    # Type of video. One of following:
    # - Trailer
    type = models.TextField()

    def __str__(self):
        return '%s (%s)' % (self.name, self.type)

class Watchlist(models.Model):
    """
    Saves movies to user watchlist
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return '%s (%s)' \
               % (self.movie.title, self.user.username)

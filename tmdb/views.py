import random

import datetime
from django.db.models import F, ExpressionWrapper, DecimalField
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from django.views import View
from django.shortcuts import redirect

from rs.models import Vote
from rs.recommenders import UserBased, ContentBased
from tmdb.models import *


class HomePageView(TemplateView):
    template_name = "tmdb/home.jinja2"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['user_based_recommendations'] = UserBased().get(self.request.user) \
                                                        .filter(vote_average__gt=4)[:100]
            start_pos = random.randint(0, len(context['user_based_recommendations']) - 6)
            context['user_based_recommendations'] = context['user_based_recommendations'][start_pos:start_pos+6]
            user_based_ids = []
            for movie in context['user_based_recommendations']:
                user_based_ids += [movie.id]
            context['content_based_recommendations'] = ContentBased().get(self.request.user) \
                                                           .filter(vote_average__gt=4) \
                                                           .exclude(id__in=user_based_ids)[:100]
            # start_pos = random.randint(0, len(context['content_based_recommendations']) - 6)
            start_pos = random.randint(0, len(context['content_based_recommendations']) - 6) if len(context['content_based_recommendations']) > 0 else 0

            context['content_based_recommendations'] = context['content_based_recommendations'][start_pos:start_pos+6]
        context['popular_movies'] = Movie.objects \
            .prefetch_related('genres').annotate(
            order_value=ExpressionWrapper(F('vote_average') - 1000 / F('vote_count'), output_field=DecimalField())) \
            .order_by('-order_value')[:100]
      #  start_pos = random.randint(0, len(context['popular_movies']) - 6)
        start_pos = random.randint(0, len(context['popular_movies']) - 6) if len(context['popular_movies']) > 0 else 0
        context['popular_movies'] = context['popular_movies'][start_pos:start_pos+12]
        return context


class MovieListView(ListView):
    model = Movie
    template_name = 'tmdb/movie_list.jinja2'
    paginate_by = 36

    def get_queryset(self):
        blocked = Vote.objects.filter(
                user=self.request.user,
                value = 0
            ).values('movie_id')
        return super(MovieListView, self).get_queryset() \
            .prefetch_related('genres').annotate(
            order_value=ExpressionWrapper(F('vote_average') - 1000 / F('vote_count'), output_field=DecimalField())) \
            .order_by('-order_value').exclude(id__in=blocked)

    def get_context_data(self, **kwargs):
        context = super(MovieListView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            return context

        context['vote_dict'] = {}
        for movie in context['object_list']:
            try:
                context['vote_dict'][movie.pk] = Vote.objects.get(
                    user=self.request.user,
                    movie=movie
                ).value
            except Vote.DoesNotExist:
                pass

        return context


class MovieDetailView(DetailView):
    model = Movie
    template_name = 'tmdb/movie.jinja2'
    context_object_name = 'movie'

    def get_queryset(self):
        return super(MovieDetailView, self).get_queryset() \
            .prefetch_related('genres', 'companies')

    def get_context_data(self, **kwargs):
        context = super(MovieDetailView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            return context

        try:
            context['vote'] = Vote.objects.get(
                user=self.request.user,
                movie=context['movie']
            ).value
        except Vote.DoesNotExist:
            pass

        try:
            context['in_watchlist'] = Watchlist.objects.get(
                user=self.request.user,
                movie=context['movie']
            )
        except Watchlist.DoesNotExist:
            pass

        try:
            context['in_blacklist'] = Vote.objects.filter(
                user=self.request.user,
                movie__id=context['movie'].id,
                value = 0
            )
        except Watchlist.DoesNotExist:
            pass

        return context


class WatchlistView(View):
    def post(self, request, movie_id):
        try:
            object = Watchlist.objects.get(
                user=request.user,
                movie__id=movie_id
            )
            Watchlist.objects.filter(
                user=request.user,
                movie__id=movie_id,
            ).delete()
            if request.is_ajax():
                return HttpResponse('removed')

        except Watchlist.DoesNotExist:
            created = Watchlist.objects.update_or_create(
                user=request.user,
                movie__id=movie_id,
                defaults={
                    'movie_id': movie_id,
                },
            )
            if request.is_ajax():
                return HttpResponse('added')
            pass

        nxt = request.POST.get('next', '')
        if not nxt:
            nxt = request.META.get('HTTP_REFERER')

        return redirect(nxt)


class WatchlistListView(ListView):
    model = Watchlist
    template_name = 'tmdb/watchlist.jinja2'

    def get_queryset(self):
        blocked = Vote.objects.filter(
                user=self.request.user,
                value = 0
            ).values('movie_id')
        return super(WatchlistListView, self).get_queryset() \
            .filter(user=self.request.user) \
            .exclude(movie_id__in=blocked)

class BlacklistView(View):
    def post(self, request, movie_id):
        try:
            object = Vote.objects.get(
                user = request.user,
                movie__id=movie_id
            )
            if object.value != 0:
                Vote.objects.update_or_create(
                    user=request.user,
                    movie__id=movie_id,
                    defaults={
                        'movie_id': movie_id,
                        'value': 0,
                    },
                )
                if request.is_ajax():
                    return HttpResponse('blocked')
            else:
                Vote.objects.filter(
                    user=request.user,
                    movie__id=movie_id,
                ).delete()
            if request.is_ajax():
                return HttpResponse('unblocked')

        except Vote.DoesNotExist:
            Vote.objects.update_or_create(
                user=request.user,
                movie__id=movie_id,
                defaults={
                    'movie_id': movie_id,
                    'value': 0,
                },
            )
            if request.is_ajax():
                return HttpResponse('created and blocked')
            pass

        nxt = request.POST.get('next', '')
        if not nxt:
            nxt = request.META.get('HTTP_REFERER')

        return redirect(nxt)

class BlacklistListView(ListView):
    model = Movie
    template_name = 'tmdb/blacklist.jinja2'
    paginate_by = 36

    def get_queryset(self):
        blocked = Vote.objects.filter(
            user=self.request.user,
            value=0
        ).values('movie_id')

        return super(BlacklistListView, self).get_queryset() \
            .filter(id__in=blocked)

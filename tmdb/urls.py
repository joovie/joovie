from django.conf.urls import url
from tmdb.views import *

urlpatterns = [
    url(r'^$', HomePageView.as_view(),
        name='home'),
    url(r'^movie/(?P<pk>\d+)/$', MovieDetailView.as_view(),
        name='movie'),
    url(r'^movies/$', MovieListView.as_view()),
    url(r'^movies/page/(?P<page>\d+)/$', MovieListView.as_view(),
        name='movie_list'),
    # watchlist action
    url(r'^movie/(?P<movie_id>\d+)/watchlist_action', WatchlistView.as_view(), name='watchlist_action'),
    url(r'^user/watchlist', WatchlistListView.as_view(), name='watchlist'),
    # blacklist action
    url(r'^movie/(?P<movie_id>\d+)/blacklist_action', BlacklistView.as_view(), name='blacklist_action'),
    url(r'^user/blacklist', BlacklistListView.as_view(), name='blacklist'),
]

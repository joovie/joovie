from django.contrib import admin
from tmdb.models import *

admin.site.register(Company)
admin.site.register(Country)
admin.site.register(Character)
admin.site.register(Genre)
admin.site.register(Keyword)
admin.site.register(Movie)
admin.site.register(Video)

"""TMDb database updater

Script that creates objects in joovie database according to data fetched via TMDb API.
Runs outside the project via command:
    python updater.py [<PATH-TO-SETTINGS-MODULE>]
<PATH-TO-SETTINGS-MODULE> is set for local in default
"""
import os
import requests
import django
import time
import sys

# Django init
path = os.path.abspath(os.path.join(os.path.join(os.path.realpath(__file__), os.pardir), os.pardir))
if path not in sys.path:
    sys.path.append(path)
SETTINGS_PATH = 'joovie.settings.local'
if len(sys.argv) > 1:
    SETTINGS_PATH = sys.argv[1]
os.environ.setdefault('DJANGO_SETTINGS_MODULE', SETTINGS_PATH)
django.setup()

from tmdb.models import Movie, Video, Character, Genre, Keyword, Company, Country

if __name__ == '__main__':
    settings = __import__(SETTINGS_PATH, fromlist=['TMDB_API_KEY'])
    last_request = requests.get(
        'https://api.themoviedb.org/3/movie/latest?api_key=%s&language=en-US' % settings.TMDB_API_KEY)

    last_movie_saved = Movie.objects.last()
    if last_movie_saved is None:
        last_movie_saved_id = 0
    else:
        last_movie_saved_id = last_movie_saved.id

    for id in range(last_movie_saved_id, last_request.json()['id'] + 1):
        time.sleep(1)
        current_movie = requests.get('https://api.themoviedb.org/3/movie/' + str(
            id) + '?api_key=%s&language=en-US' % settings.TMDB_API_KEY)
        if current_movie.status_code == 200:
            credits = requests.get('https://api.themoviedb.org/3/movie/' + str(
                id) + '/credits?api_key=%s' % settings.TMDB_API_KEY).json()
            keywords = requests.get('https://api.themoviedb.org/3/movie/' + str(
                id) + '/keywords?api_key=%s' % settings.TMDB_API_KEY).json()
            videos = requests.get('https://api.themoviedb.org/3/movie/' + str(
                id) + '/videos?api_key=%s&language=en-US' % settings.TMDB_API_KEY).json()
            movie_object = current_movie.json()

            if movie_object['release_date'] == '':
                release_date = None
            else:
                release_date = movie_object['release_date']

            movie = Movie(adult=movie_object['adult'], backdrop_path=movie_object['backdrop_path'],
                          budget=movie_object['budget'], homepage=movie_object['homepage'], id=movie_object['id'],
                          imdb_id=movie_object['imdb_id'], original_language=movie_object['original_language'],
                          original_title=movie_object['original_title'], overview_en=movie_object['overview'],
                          popularity=movie_object['popularity'], poster_path=movie_object['poster_path'],
                          release_date=release_date, revenue=movie_object['revenue'],
                          runtime=movie_object['runtime'], status=movie_object['status'], title=movie_object['title'],
                          vote_average=movie_object['vote_average'], vote_count=movie_object['vote_count'])
            movie.save()

            for trailer_json in videos['results']:
                video = Video(id=trailer_json['id'], key=trailer_json['key'], name=trailer_json['name'],
                              site=trailer_json['site'], size=trailer_json['size'], type=trailer_json['type'],
                              movie=movie)
                video.save()

            for character_json in credits['cast']:
                character = Character(movie=movie, character=character_json['character'], actor=character_json['name'],
                                      order=character_json['order'], picture=character_json['profile_path'])
                character.save()

            for genre_json in movie_object['genres']:
                genre, _ = Genre.objects.get_or_create(id=genre_json['id'], name=genre_json['name'])
                movie.genres.add(genre)

            for keyword_json in keywords['keywords']:
                try:
                    keyword, _ = Keyword.objects.get_or_create(id=keyword_json['id'], name=keyword_json['name'])
                    movie.keywords.add(keyword)
                except Exception as e:
                    pass

            for company_json in movie_object['production_companies']:
                company, _ = Company.objects.get_or_create(id=company_json['id'], name=company_json['name'])
                movie.companies.add(company)

            for country_json in movie_object['production_countries']:
                country, _ = Country.objects.get_or_create(code=country_json['iso_3166_1'], name=country_json['name'])
                movie.countries.add(country)

# The Movie Database

`tmdb` is the [The Movie Database][tmdb] adaptation for Joovie.
The main role of application is *storing* and *presentation*
of database elements such as movies, actors etc.


[tmdb]: https://www.themoviedb.org "The Moovie Database website"

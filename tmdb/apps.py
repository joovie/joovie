from django.apps import AppConfig


class TmdbConfig(AppConfig):
    name = 'tmdb'
    verbose_name = 'The Movie Database'

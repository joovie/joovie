(function () {
  function modal(element) {
    function showModal() {
      document.body.classList.add('in-modal');
      element.classList.add('in');

      document.body.addEventListener('click', closeModal);
      document.addEventListener('keyup', escapeModal);
    }

    function hideModal() {
      document.body.classList.remove('in-modal');
      element.classList.remove('in');
      element.innerHTML = '';

      document.removeEventListener('click', closeModal);
      document.removeEventListener('keyup', escapeModal);
    }

    function closeModal(event) {
      if (event.target !== this) return true;

      hideModal();
    }

    function escapeModal(event) {
      if (event.keyCode === 27) hideModal();
    }

    showModal(element);
  }

  function showTrailer(site, key) {
    if (site === 'YouTube') {
      var videoURL = 'https://www.youtube.com/embed/' + key +
        '?enablejsapi=1&showinfo=0&color=white';

      var modalElement = document.querySelector('.movie-modal');
      modalElement.innerHTML = '<iframe id="player" type="text/html" src="' +
        videoURL + '" allowfullscreen></iframe>';

      modal(modalElement);
    }
  }

  document.addEventListener('load', function () {
    document.querySelectorAll('.movie-trailer-link').forEach(function (link) {
      console.log(link);
      link.onclick = function (event) {
        event.preventDefault();

        var site = this.getAttribute('data-site');
        var key = this.getAttribute('data-key');
        showTrailer(site, key);

        return false;
      };
    });
  }, true);
})();

/* Utils */
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';');
    for (let cookie of cookies) {
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }

  return cookieValue;
}

function processActionWatchlist(form, onsuccess, onerror) {
  console.log(form)
  let oRequest = new XMLHttpRequest();

  oRequest.onload = () => {
    console.log(oRequest.responseText);
    if (oRequest.status === 200 && onsuccess) {
      onsuccess(oRequest);
    } else if (onerror) {
      onerror(oRequest);
    }
  };

  oRequest.open('POST', form.action);
  oRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  oRequest.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
  oRequest.send(new FormData(form));
}

function initWatchlistForms() {
  var form = document.querySelector('.watchlist-action-form');
  var input_watchlist = document.querySelector('.watchlist-action-submit');
  form.querySelector('.watchlist-action-submit').onclick = (event) => {
    event.preventDefault();
    processActionWatchlist(form, (oReq) => {
      const result = oReq.responseText;
      if (result == 'added') {
        input_watchlist.value = "Delete from watchlist"
      } else {
        input_watchlist.value = "Add to watchlist"
      }
    });
  };
}
document.addEventListener('load', initWatchlistForms, true);

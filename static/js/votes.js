/* Utils */
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';');
    for (let cookie of cookies) {
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
/* Voting */
function processVote(form, onsuccess, onerror) {
  let oRequest = new XMLHttpRequest();
  oRequest.onload = () => {
    console.log(oRequest.responseText);
    if (oRequest.status === 200 && onsuccess) {
      onsuccess(oRequest);
    } else if (onerror) {
      onerror(oRequest);
    }
  };
  oRequest.open('POST', form.action);
  oRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  oRequest.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
  oRequest.send(new FormData(form));
}
function initVoteForms() {
  var input_blacklist = document.querySelector('.blacklist-action-submit');
  document.querySelectorAll('.vote-form').forEach((form) => {
    form.querySelectorAll('.vote-stars input').forEach((input) => {
      input.onchange = () => {
        input_blacklist.value = "Block";
        processVote(form);
      };
    });
  });
}
document.addEventListener('load', initVoteForms, true);

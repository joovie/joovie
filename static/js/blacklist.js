/* Utils */
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';');
    for (let cookie of cookies) {
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }

  return cookieValue;
}

function processActionBalcklist(form, onsuccess, onerror) {
  console.log(form)
  let oRequest = new XMLHttpRequest();

  oRequest.onload = () => {
    console.log(oRequest.responseText);
    if (oRequest.status === 200 && onsuccess) {
      onsuccess(oRequest);
    } else if (onerror) {
      onerror(oRequest);
    }
  };

  oRequest.open('POST', form.action);
  oRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  oRequest.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
  oRequest.send(new FormData(form));
}

function initBalcklistForms() {
  var form = document.querySelector('.blacklist-action-form');
  var input_blacklist = document.querySelector('.blacklist-action-submit');
  form.querySelector('.blacklist-action-submit').onclick = (event) => {
    event.preventDefault();
    processActionBalcklist(form, (oReq) => {
      const result = oReq.responseText;
      if (result == 'unblocked') {
        input_blacklist.value = "Block"
      } else {
        input_blacklist.value = "Unblock"
      }
    });
  };
}
document.addEventListener('load', initBalcklistForms, true);

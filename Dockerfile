FROM python:3

ENV PYTHONUNBUFFERED 1
ENV WORKDIR /joovie

RUN mkdir -p "$WORKDIR"
WORKDIR "$WORKDIR"

ADD requirements.txt "$WORKDIR/"

RUN pip install -r requirements.txt
ADD . "$WORKDIR/"

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

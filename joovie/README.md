# Joovie

Module `joovie` is the main application of Joovie Project.
It contains all bootstrap configuration, like a *settings* and *urls*. 

Take a minute to know its explained.


## Settings

Settings are stored in separated files depended to a developer.
 
### settings.py

`settings.py` contains global project settings which are commited 
and shared with all developers and also in production version. 
It reads and includes other files, explained below.

### settings_local.py

`settings_local.py` contains your local setting like a connection 
to local database server. You can override any of global settings here.

Also you can pass *secret key* here and do not create `.secret` file.

### secret.key

`secret.key` file stores secret production key which must not be commited. 

## URLs

`urls.py` stores bootstrap URL configuration of the project. 
It includes other application's urls.

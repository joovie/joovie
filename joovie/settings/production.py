"""
Django production settings for joovie project

This file stores general production settings for Joovie project.
Also included loading of passwords and secret keys from 'key-galleries'

For more information on Django settings, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""
from .base import *


# Take a look at production checklist to verify this.
# https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/


SECRET_KEY = open('/etc/joovie/secret.key').read().strip()

DEBUG = False

ALLOWED_HOSTS = [
    'joovie.pp.ua'
]


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'joovie',
#         'USER': 'joovie',
#         'PASSWORD': open('/etc/joovie/mysql.key').read().strip(),
#         'HOST': '127.0.0.1',
#         'PORT': '3306',
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'joovie',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '0.0.0.0:8000',
        'PORT': '3306',
    }
}

# TMDB API credentials
TMDB_API_KEY = '8a257d9f7c09af0a5e492962ed5ac93c'


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

"""
Django development settings for joovie project

This file stores all development settings of Joovie project
such as debug, localhost and so on.

For more information on Django settings, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""
from .base import *


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

SECRET_KEY = 'quuuk(n%szv5pfobn0(n_#&9=#%!qxa56lu0hha1!a1)-#auq0'

DEBUG = True

ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
    '*',
]


# Application definition

TEMPLATES.append(
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
)


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
#
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'joovie',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '35.192.116.180',
        'PORT': '3306',
    }
}


# TMDB API credentials
TMDB_API_KEY = '8a257d9f7c09af0a5e492962ed5ac93c'


# Emails
# https://docs.djangoproject.com/en/1.10/ref/settings/#email-backend

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'team.joovie@gmail.com'
EMAIL_HOST_PASSWORD = '1234qwer1234'
SERVER_EMAIL = 'team.joovie@gmail.com'
DEFAULT_FROM_EMAIL = 'Joovie Team'

# Joovie

The movie recommendation service based on [TMDb][tmdb]
and stack of recommendation algorithms with different approaches.


## Quick start

### Prerequisites

To start working you need to have installed [Docker CE][docker-install]
and [docker-compose][docker-compose-install].

Also download test database from our repository with DB dumps and save it
with name `db.sqlite3`.


### Running

Run development environment with simple command inside of repository root:

```bash
docker-compose up
```

To stop service use:

```bash
docker-compose down
```

Quite simple, right? Nice contributing!


## Structure

The project contains following list of applications:

- [`joovie`](./joovie) — bootstrap project application
- [`tmdb`](./tmdb) — [The Movie Database][tmdb] representation

Each one is explained in its directory.

---

Also are planned:

- `rs` — Recommendation System
- `registration` — [django-registration][django-registration]
  application by *ubernostrum*


[tmdb]: https://www.themoviedb.org "The Moovie Database website"
[django-registration]: https://django-registration.readthedocs.io
[venv]: https://virtualenv.pypa.io/en/stable/ "Virtualenv docs"
[docker-install]: https://docs.docker.com/engine/installation/
[docker-compose-install]: https://docs.docker.com/compose/install/
